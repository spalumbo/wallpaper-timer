#!/bin/bash
USER=$(whoami)
TIME_24HOUR=$(date +%T)
TIME_12HOUR=$(date +%r)
echo "Wallpaper updating at: ${TIME_24HOUR} / ${TIME_12HOUR}"
echo "if (curHour < 4) then
		return lateNight of fileMap
	else if (curHour < 8) then
		return morning of fileMap
	else if (curHour < 12) then
		return lateMorning of fileMap
	else if (curHour < 15) then
		return afternoon of fileMap
	else if (curHour < 18) then
		return lateAfternoon of fileMap
	else if (curHour < 19) then
		return evening of fileMap
	else if (curHour < 21) then
		return lateEvening of fileMap
	else
		return night of fileMap
	end if"
osascript /Users/${USER}/bin/wallpaper-timer.scpt
echo "script executed"
