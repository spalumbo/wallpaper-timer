#Images must be in place before doing this.
#See wallpaper-timer.scpt for image locations.
#wallpaper-timer.scpt is an applescript file.

cp -rf bin/*t  ~/bin/
cp -rf Library/LaunchAgents/com.steve.wallpapertimer.plist ~/Library/LaunchAgents/
cd ~/Library/LaunchAgents/
launchctrl load com.steve.wallpapertimer.plist #or reboot
